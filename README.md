# gatling.io setup notes

- Enrol in the [Gatling Academy](https://academy.gatling.io/) and review the introdutory guide.
- Install a recommended Java version. E.g. 1.8 or 11
- Install a recommended scala version, e.g [2.12.10](https://www.scala-lang.org/download/2.12.10.html)
- install maven "brew install maven"
- [download gatling open source](https://gatling.io/open-source/start-testing/)
- Direct download link
```
https://repo1.maven.org/maven2/io/gatling/highcharts/gatling-charts-highcharts-bundle/${GATLING_VERSION}/gatling-charts-highcharts-bundle-${GATLING_VERSION}-bundle.zip
```
- unzip the bundle and create a symbolic link name gatling -> bundle directory
